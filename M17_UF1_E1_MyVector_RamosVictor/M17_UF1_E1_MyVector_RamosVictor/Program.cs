﻿using System;

namespace M17_UF1_E1_MyVector_RamosVictor
{
    class Program
    {
        static void Main()
        {
            

            bool exit = false;
            var name = new Name();
            var vecgame = new VectorGame();
            var vector = new MyVector();
            while (!exit)
            {         
                Console.WriteLine("///////////////////////////////////////////");
                Console.WriteLine("Escoje la acción que quieras hacer ");
                Console.WriteLine("[1] Salu2");
                Console.WriteLine("[2] My Vector");
                Console.WriteLine("[3] VectorGame");
                Console.WriteLine("[0] Salir");
                Console.WriteLine("///////////////////////////////////////////");
                int select = Convert.ToInt32(Console.ReadLine());
                switch (select)
                {
                    case 1:
                  
                        Console.WriteLine(name);
                        break;
                    case 2:
                        Console.WriteLine("Introduce el punto inicial del vector: ");
                        int inicial = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Introduce el punto final del vector: ");
                        int fin = Convert.ToInt32(Console.ReadLine());
                        vector.ToString();
                        
                        break;
                    case 3:
                        Console.WriteLine("Introdueix quants vectors random vols: ");
                        int numV = Convert.ToInt32(Console.ReadLine());
                        vecgame.randomV(numV);
                        break;
                    case 0:
                        Console.WriteLine("Saliendo");
                        exit = false;
                        break;
                    default:
                        Console.WriteLine("Caso no existente");
                        break;
                }
            }

        }


    }
    
   
}
