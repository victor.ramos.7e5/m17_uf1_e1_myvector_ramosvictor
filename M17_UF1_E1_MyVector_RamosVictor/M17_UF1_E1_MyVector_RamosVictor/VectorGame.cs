﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Intrinsics;
using System.Text;

namespace M17_UF1_E1_MyVector_RamosVictor
{
    class VectorGame
    {

        public VectorGame() { }
         public MyVector[] randomV(int numV)
         {
             MyVector[] arrayV = new MyVector[numV];
             for (int i = 0; i < arrayV.Length; i++) 
            { 
                arrayV[i] = new MyVector(new int[5], new int[5]);
            }
             return arrayV;
         }
        public MyVector[] SortVectors(MyVector[] arrayV)
        {
            var ordenado = arrayV.OrderBy(x => x).ToArray();
            return ordenado;
           }
        public MyVector[] SortVectorsdesc(MyVector[] arrayV)
        {
            var ordenado = arrayV.OrderByDescending(x => x).ToArray();
            return ordenado;
        }
    }
   }
    

