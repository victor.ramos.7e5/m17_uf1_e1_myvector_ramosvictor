﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_RamosVictor
{
    class MyVector
    {

        /*
         * Crea una classe amb nom MyVector. Implenta els següent requisits:
            Guarda i gestiona vectors de dos dimensions
            Variable on es guarda les coordenades 2D d’un vector. Punt d’inici i punt final.
            Funcions get i set 
            Funcions que retornen la distancia del vector
            Funció que canvia la sentido del vector
            Funció .toString()
         */
        int[] x0;
        int[] y0;

        public static Array CompareByProximity { get; internal set; }
        public static Array CompareByLength { get; internal set; }

        public MyVector(int[] inicio, int[] fin)
        {
            x0 = inicio;
            y0 = fin;
        }

        public MyVector()
        {
        }

        public void setInici(int[] inicio)
        {
            x0 = inicio;
        }

        public int[] getInici()
        {
            return x0;
        }
        public void setFinal(int[] fin)
        {
            y0 = fin;
        }

        public int[] getFinal()
        {
            return y0;
        }


        public double LengthV()
        {
            double dist = 0.0;
            dist = Math.Sqrt(Math.Pow(Math.Abs(x0[0] - y0[0]), 2) + Math.Pow(Math.Abs(y0[1] - y0[1]), 2));
            return dist;
        }
        public void changeDir()
        {
            x0 = new int[] { x0[0] * -1, x0[1] * -1 };
            y0 = new int[] { y0[0] * -1, y0[1] * -1 };
        }
        public override string ToString()
        {
            return "PUNT INICIAL VECTOR: [" + x0[0] + ", " + x0[1] + "]" +
            "PUNT FINAL VECTOR: [" + y0[0] + ", " + y0[1] + "]";
        }


    }
}
